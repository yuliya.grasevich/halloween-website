# Halloween Website

Implement design according [Figma Design](<https://www.figma.com/file/Z9i2HiOV3VtvhnqkKBUUon/Free-Halloween-Party-Time-Landing-Page-(Community)?type=design&node-id=0-1&mode=design&t=rp5neXS05UbP5lGk-0>)

## Requirements and scoring (maximum 10 points)

### General project issues - 3 points

- Use Vite
- Use TypeScript
- Use HTML5
  - Optional: Handlebars, EJS, Pug (if in use, please be prepared for theory questions to all team about chosen technology)
- Download assets from Figma and use in the project
  - if there is no any asset (like stylish fonts), replace with any font close to the missing one (https://fonts.google.com/)
- Optional: deploy to Gitlab Pages (if you have valid EU credit card)

### Header, Hero, Footer sections - 1 point

- Implemented Figma sections
  - Header (Party time, navbar, reservation button, locales buttons)
  - Hero (Its Halloween party o'clock with main image with pumpkins)
  - Footer (Phone reservation with bats colored in orange and gray flying over the gray grass)

### Images sections - 1 point

- Implemented Figma sections
  - Halloween memories (with images)
    - When user hover mouse cursor over an image, that image should be enlarged

### Texts section - 1 point

- Implemented Figma sections
  - Join us This year's halloween party

### ECommerce section - 1 point

- Implemented Figma sections
  - Let's be your hosts (with cards)
    - When user hover mouse cursor over a card or focus a card, card should be painted in orange wuth black button
    - When user move mouse cursor out of a card or a card loses focus, card should be painted in black with orange button

### Localization - 1 point

- Add localization (two languages EN and AR)
  - For loading locales please add mock request to backend (promise and setTimeout)
- Support RTR/LTL versions (content from right to left for AR translation)

### Code quality - 1 point

- Use Prettier (https://prettier.io/)
- Use ESLint (https://typescript-eslint.io/)
  - Optional: use Husky pre commit hooks (https://typicode.github.io/husky/)

### Git Flow - 1 point

- List of issues in forked gitlab repo
- Each issue has short and informative name
- Each issue has its own unique number
- List of merge requests
- Each merge request has description of what that MR is about and approvals from team members (green check marks)
