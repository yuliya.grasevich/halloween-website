import "./footer.css";
import BehanceIcon from "../../assets/icons/behance.png";
import FigmaIcon from "../../assets/icons/Figma.png";
import LinkedInIcon from "../../assets/icons/LinkedIn.png";
import InstagramIcon from "../../assets/icons/instagram.png";
import YouTubeIcon from "../../assets/icons/youtube.png";

const TEMPLATE = `
      <div class="footer-content">
        <h2 class="heading heading--1"><span localization-key="footer-heading">phone reservation? </span><a href="tel:+19874652">(+1) 987 46 52</a></h2>
        <div class="social-links">
            <a href="https://www.behance.net/" aria-label="Link to Behance" target="_blank"><img src="${BehanceIcon}" alt="Behance"></a>
            <a href="https://www.figma.com/" aria-label="Link to Figma" target="_blank"><img src="${FigmaIcon}" alt="Figma"></a>
            <a href="https://www.linkedin.com/" aria-label="Link to LinkedIn" target="_blank"><img src="${LinkedInIcon}" alt="LinkedIn"></a>
            <a href="https://www.instagram.com/" aria-label="Link to Instagram" target="_blank"><img src="${InstagramIcon}" alt="Instagram"></a>
            <a href="https://www.youtube.com/" aria-label="Link to YouTube" target="_blank"><img src="${YouTubeIcon}" alt="Youtube"></a>
        </div>
      </div>
    `;

export const Footer = (): HTMLElement => {
  const footer = document.createElement("footer");
  footer.id = "footer";
  footer.classList.add("footer");

  footer.innerHTML = TEMPLATE;

  return footer;
};
