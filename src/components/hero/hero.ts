import "./hero.css";
import calendarIcon from "../../assets/icons/calendar.png";
import arrowDownIcon from "../../assets/icons/arrow-down.svg";

const TEMPLATE = `
      <div class="hero-data">
        <img src=${calendarIcon} alt="calendar">
        <p class="hero-text-date" localization-key="hero-text-date">31 Octobr 2023</p>
      </div>
      <h2 class="hero-title" localization-key="hero-title">
        It's Halloween Party O'Clock!
      </h2>
      <a class="hero-arrow" href="#ecommerce">
       <img src=${arrowDownIcon} alt="arrow-down">
      </a>
    `;

export const Hero = (): HTMLElement => {
  const hero = document.createElement("section");
  hero.id = "hero";
  hero.classList.add("hero");

  hero.innerHTML = TEMPLATE;

  return hero;
};
