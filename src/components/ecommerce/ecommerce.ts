import "./ecommerce.css";
import { CardItem } from "./card-item/card-item";

const TEMPLATE = `
    <h2 class="heading heading--1" localization-key="ecommerce-heading">
    Let's be your hosts
    </h2>
    <div class="spiderweb-img"></div>
    <div class="ecommerce-cards">
      ${CardItem(1)}    
      ${CardItem(2)}
      ${CardItem(3)}  
    </div>
  `;

export const Ecommerce = () => {
  const ecommerceElement = document.createElement("section");

  ecommerceElement.classList.add("ecommerce-section");
  ecommerceElement.id = "ecommerce";
  ecommerceElement.innerHTML = TEMPLATE;

  return ecommerceElement;
};
