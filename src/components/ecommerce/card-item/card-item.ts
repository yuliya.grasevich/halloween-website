import "./card-item.css";

export const CardItem = (cardNumber: number): string => {
  return `
    <div class="ecommerce-card-item">
        <h4 class="ecommerce-card-item-title" localization-key="ecommerce-card-item-title-${cardNumber}"></h4>
        <p class="ecommerce-card-item-price" localization-key="ecommerce-card-item-price-${cardNumber}"></p>
        <p localization-key="ecommerce-card-item-drink-${cardNumber}"></p>
        <p localization-key="ecommerce-card-item-activity-${cardNumber}"></p>
        <p localization-key="ecommerce-card-item-addon-${cardNumber}"></p>
        <button class="ecommerce-card-item-btn" localization-key="ecommerce-card-item-button-${cardNumber}">Buy Ticket</button>        
    </div>
  `;
};
