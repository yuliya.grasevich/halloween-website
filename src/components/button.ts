import "./button.css";

type ButtonType = "small" | "big";

interface ButtonProps {
  btnType: ButtonType;
  text: string;
  localizationKey: string;
  link: string;
}

export default function ButtonComp(props: ButtonProps): HTMLAnchorElement {
  const { btnType, text, localizationKey, link } = props;

  const linkElement = document.createElement("a");
  linkElement.href = link;

  const button = document.createElement("button");
  button.innerHTML = `${text}`;

  if (btnType === "small") {
    button.classList.add("small-btn");
  } else if (btnType === "big") {
    button.classList.add("big-btn");
  }

  button.setAttribute("localization-key", localizationKey);

  linkElement.appendChild(button);

  return linkElement;
}
