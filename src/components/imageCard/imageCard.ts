import { getSectionImageUrl } from "../../utils/utils.ts";
import "./imageCard.css";

export const ImageCard = (img: string, index: number) => {
  const imgUrl = getSectionImageUrl(img);

  return `
      <div class="card-image">
         <img src="${imgUrl}" alt="img-${index}">
      </div>
    `;
};
