import "./about.css";
import ticketImgSrc from "../../assets/icons/ticket.svg";
import locationImgSrc from "../../assets/icons/location.svg";
import { dataAboutSection } from "../../utils/data";
import { TextItem } from "./text-item/text-item";

const TEMPLATE = `
      <h2 class="about-title" localization-key="about-title">
        join us
      </h2>
      <h2 class="about-title-red" localization-key="about-title-red">
        this year’s Halloween Party!
      </h2>
      <p class="about-text" localization-key="about-text">Our Halloween party this year will be an unforgettable experience, filled with spooky decorations, eerie music, thrilling games, and costume contests. Join us for a night of magic and fright!</p>
      <div class="about-ticket">
        <div class="about-ticket-img"><img src=${ticketImgSrc} alt="ticket-icon"></div>
        <div class='about-ticket-data'>
          <p localization-key="about-ticket-data-text">Tue, 31 Octobr 2023, 19:00</p>
          <div class='ticket-data-location'>
            <img src=${locationImgSrc} alt="location-icon">
            <p localization-key="about-ticket-data-location">The Menza Club, Istanbul city</p>
          </div>
        </div>
      </div> 
      <div class="about-items">
        ${dataAboutSection
          .map((elem) =>
            TextItem({ text: elem.text, className: elem.className }),
          )
          .join("")}
      </div>
      <div class="spider-img"></div>
`;

export const About = (): HTMLElement => {
  const about = document.createElement("section");
  about.id = "about";
  about.classList.add("about-section");
  about.innerHTML = TEMPLATE;
  return about;
};
