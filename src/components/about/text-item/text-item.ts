import "./text-item.css";
import imgIcon from "../../../assets/icons/tick-square.svg";

type TextItemProps = {
  text: string;
  className: string;
};

export const TextItem = ({ text, className }: TextItemProps): string => {
  return `
    <div class="text-item ${className}">
       <div class='item-img'><img src=${imgIcon} alt="tick-square"></div><p localization-key="about-${className}">${text}</p>
    </div>
  `;
};
