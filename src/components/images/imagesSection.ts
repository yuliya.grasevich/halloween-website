import "./imagesSection.css";
import { ImageCard } from "../imageCard/imageCard.ts";

export function ImagesSection(data: string[]) {
  const section = document.createElement("section");

  section.classList.add("section");
  section.id = "images";
  section.innerHTML = `
        <div class="bg-el bg-el--bat"></div>
        <div class="container--image">
            <div class="heading-section">
                <h2 class="heading heading--1" localization-key="images-heading">Halloween Memories</h2>
            </div>
            <div class="grid grid--images">
                ${data.map((item, index) => ImageCard(item, index)).join(" ")}
            </div>
        </div>
    `;

  return section;
}
