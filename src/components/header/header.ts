import "./header.css";
import ButtonComp from "../button";

const TEMPLATE = `

    <h1 class="logo" localization-key="header-title">Party Time!</h1>

    <div class="menu-list">
        <ul class="menu-list_body">
          <li><a href="#hero" localization-key="header-menu-1">Home</a></li>
          <li><a href="#images" localization-key="header-menu-2">Gallery</a></li>
          <li><a href="#about" localization-key="header-menu-3">About Party</a></li>
          <li><a href="#ecommerce" localization-key="header-menu-4">Reservation</a></li>
          <li><a href="#footer" localization-key="header-menu-5">Contacts</a></li>
        </ul>
    </div>
 
    <div class="language-switcher">
       <button language-switcher id="en" type="button">EN</button>
       <button language-switcher id="ar" type="button">عرب</button>
    </div>             
`;

export const Header = () => {
  const headerElement = document.createElement("header");

  headerElement.classList.add("header");
  headerElement.innerHTML = TEMPLATE;

  const reservationButton = ButtonComp({
    btnType: "small",
    text: "Reservation",
    localizationKey: "header-button",
    link: "#ecommerce",
  });
  headerElement.append(reservationButton);

  return headerElement;
};
