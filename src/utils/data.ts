export const dataImageSection = [
  "image01.png",
  "image02.png",
  "image03.png",
  "image04.png",
  "image05.png",
  "image06.png",
  "image07.png",
  "image08.png",
  "image09.png",
  "image10.png",
];

export const dataAboutSection = [
  {
    text: "NightWitch hunt",
    className: "item-1",
  },
  {
    text: "Bestcostume contest",
    className: "item-2",
  },
  {
    text: "DeliciousCakes and sweets",
    className: "item-3",
  },
  {
    text: "Everybody get tipsy",
    className: "item-4",
  },
  {
    text: "Dance-off with a star guest",
    className: "item-5",
  },
];
