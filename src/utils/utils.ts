export function getSectionImageUrl(img: string) {
  return new URL(`/src/assets/images/imageSection/${img}`, import.meta.url)
    .href;
}
