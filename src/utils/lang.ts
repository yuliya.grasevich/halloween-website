import ar from "../lang/ar.json";
import en from "../lang/en.json";

interface Translation {
  [key: string]: string;
}

const defaultLanguage = "en";
let language: string;
let translations: Translation = {};

// set default language
setTimeout(() => {
  setLanguage(defaultLanguage);
  bindLanguageSwitcher();
}, 0);

// add event listener for language switchers
function bindLanguageSwitcher(): void {
  const switcher = document.querySelectorAll("[language-switcher]");
  switcher.forEach((s) => {
    s.addEventListener("click", (e: Event) => {
      setLanguage((<Element>e.target).id);
    });
  });
}

// change translation and direction
async function setLanguage(newLanguage: string): Promise<void> {
  if (newLanguage === language) return;

  if (newLanguage) {
    const newTranslations: Translation = await fetchTranslations(newLanguage);

    language = newLanguage;
    translations = newTranslations;

    document.documentElement.dir = direction(newLanguage);
    document.documentElement.lang = newLanguage;

    translatePage();
  }
}

// get necessary translation
function fetchTranslations(newLocale: string): Promise<Translation> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(newLocale === "ar" ? ar : en);
    }, 0);
  });
}

// translate all page
function translatePage(): void {
  document.querySelectorAll("[localization-key]").forEach(translateElement);
}

// translate exact element
function translateElement(element: Element): void {
  const key = element.getAttribute("localization-key");
  element.textContent = translations[key || ""];
}

// determine the direction
function direction(locale: string): string {
  return locale === "ar" ? "rtl" : "ltr";
}
