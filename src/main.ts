import "./normalize.css";
import "./style.css";
import "./utils/lang";

import { Header } from "./components/header/header";
import { Hero } from "./components/hero/hero";
import { ImagesSection } from "./components/images/imagesSection.ts";
import { dataImageSection } from "./utils/data.ts";
import { About } from "./components/about/about";
import { Ecommerce } from "./components/ecommerce/ecommerce";
import { Footer } from "./components/footer/footer.ts";

document.addEventListener("DOMContentLoaded", () => {
  const app = document.querySelector<HTMLDivElement>("#app");

  if (app) {
    // Header section
    const header = Header();
    app.append(header);

    // Hero section
    const hero = Hero();
    app.append(hero);

    // Images section
    const images = ImagesSection(dataImageSection);
    app.append(images);

    // About section
    const about = About();
    app.append(about);

    // E-commerce section
    const ecommerce = Ecommerce();
    app.append(ecommerce);

    // footer section
    const footer = Footer();
    app.append(footer);
  }
});
